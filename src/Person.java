
public abstract class Person implements java.io.Serializable{

    private Adress adress;
    private String name;
    private int birthYear;

    public Person(String name, int birthYear) {
        this(name, birthYear, Adress.getDefault());
    }

    public Person(String name, int birthYear, Adress adress) {
        this.adress = adress;
        this.name = name;
        this.birthYear = birthYear;
    }

    @Override
    public String toString() {
        return name + ", year: " + birthYear + ", adress: " + adress.get();
    }
}
