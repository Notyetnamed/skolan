
public class Student extends Person implements java.io.Serializable {

    private float avgGrades;

    public Student(String name, int birthYear) {
        super(name, birthYear);

        this.avgGrades = 5;
    }

    @Override
    public String toString() {
        return "Student: " + super.toString() + ", grades: " + avgGrades;
    }
}
