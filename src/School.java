

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class School {
    private ArrayList<Teacher> teachers;
    private ArrayList<Student> students;
    private ArrayList<Program> programs;

    public School() {
        programs = new ArrayList<Program>();

        load();
    }

    public void addPerson(Person person) {
        if(person instanceof Student) {
            students.add((Student)person);
        }else {
            teachers.add((Teacher)person);
        }
    }

    public void printStudents() {
        printPeople(students);
    }

    public void printTeachers() {
        printPeople(teachers);
    }

    private <T extends Person>void printPeople(ArrayList<T> people) {
        for(Person person : people) {
            System.out.println(person.toString());
        }
    }

    public void addStudentToProgram(Student student, Program program) {
        // TODO: MAKE FIX
    }

    public void load() {
        students = loadPerson("students.dat");
        teachers = loadPerson("teachers.dat");

        if(students == null){
            students = new ArrayList<Student>();
        }

        if(teachers == null) {
            teachers = new ArrayList<Teacher>();
        }
    }

    private <T extends Person>ArrayList<T> loadPerson(String path){
        ArrayList<T> result = null;

        try{
            FileInputStream inputStream = new FileInputStream(path);
            ObjectInputStream objectStream = new ObjectInputStream(inputStream);
            result = (ArrayList<T>)objectStream.readObject();
            objectStream.close();
            inputStream.close();
        }catch(Exception e){
            System.out.println("fel i load");
        }

        return result;
    }

    public void save() {
        savePeople(students, "students.dat");
        savePeople(teachers, "teachers.dat");
    }

    private <T extends java.io.Serializable>void savePeople(T people, String path){

        try{
            FileOutputStream outputStream = new FileOutputStream(path);
            ObjectOutputStream stream = new ObjectOutputStream(outputStream);
            stream.writeObject(people);
            stream.close();
            outputStream.close();
        }catch(Exception e){
            System.out.println("-Save failed-");
        }

    }
}
