import java.util.Scanner;

public class TeacherFactroy extends PersonFactory implements Factory<Teacher> {

    public TeacherFactroy(Scanner scan) {
        super(scan);
    }

    @Override
    public Teacher create(){

        System.out.println("Name: ");
        //String name = super.currentScanner().nextLine();

        Teacher newTeacher = new Teacher("Leif", Subject.JAVA, Subject.PYTHON);


        return newTeacher;
    }
}
