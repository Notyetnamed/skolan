import java.util.Scanner;

public class Menu {


    private School school;
    private StudentFactory studentFact;
    private TeacherFactroy teacherFact;

    public Menu() {
        Scanner scan = new Scanner(System.in);

        school = new School();
        studentFact = new StudentFactory(scan);
        teacherFact = new TeacherFactroy(scan);
    }

    public void runMenu() {
        boolean run = false;
        Scanner scan = new Scanner(System.in);

        do{
            int input = printChoices(scan);
            run = true;

            switch (input) {
                case 1:
                    createPerson(studentFact);
                    break;
                case 2:
                    createPerson(teacherFact);
                    break;
                case 3:
                    school.printStudents();
                    break;
                case 4:
                    school.printTeachers();

                    break;
            }
        }while(run);

        school.save();
    }

    private <T extends Person>void createPerson(Factory<T> factory) {
        Person person = factory.create();
        school.addPerson(person);
    }

    private int printChoices(Scanner scan) {
        System.out.println("Hej, všlkommen");
        System.out.println("1. Add student");
        System.out.println("2. Add teacher");
        System.out.println("3. print students");
        System.out.println("4. print teachers");

        int input = scan.nextInt();
        return input;

    }
}
