import java.util.Scanner;

public class StudentFactory extends PersonFactory implements Factory<Student> {

    public StudentFactory(Scanner scan) {
        super(scan);
    }

    @Override
    public Student create(){

        System.out.print("Students name: ");
        //String name = super.currentScanner().nextLine();
        System.out.print("Year of birth: ");
        //int birthYear = Integer.parseInt(super.currentScanner().nextLine());

        Student newStudent = new Student("Kjell", 1872);

        return newStudent;
    }
}
