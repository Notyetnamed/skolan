
public class Adress implements java.io.Serializable {

    private transient /*sparas ej*/static Adress defaultAdress = null; //Lazy loading - objektet skapas OM och N�R det beh�vs, inte f�re.(Annars �r det eager loading)
    private String street;
    private  String postCode;

    private Adress() {
        this("DEFAULT", "000 00");
    }

    public Adress(String street, String postCode) {
        this.street = street;
        this.postCode = postCode;
    }

    public String get() {
        return street + " - " + postCode;
    }

    public static Adress getDefault() {
        if (defaultAdress == null) {
            defaultAdress = new Adress();
        }
        return defaultAdress;
    }
}
